Simple ELG-compliant service in Java
====================================

This is a very simple example of an ELG-compatible service implemented in Java, using the [ELG Micronaut LT Service](https://gitlab.com/european-language-grid/platform/lt-service-micronaut) helper library.  The actual service functionality is deliberately as simple as possible - it is a trivial tokeniser which simply splits the input text at sequences of one or more whitespace characters and returns a "Token" annotation for each word.

The files in this repository have almost all been generated automatically using https://micronaut.io/launch - the exceptions are the [TokeniserController](src/main/java/eu/elg/example/TokeniserController.java) itself and two small additions to [build.gradle](build.gradle) to add a dependency on the ELG helper and on the "jib" plugin for building Docker images.

To build the service into a Docker image you can run locally, use

```
./gradlew jibDockerBuild
```

The default image reference for `jibDockerBuild` is the project name and version, i.e. `simple-micronaut-tokeniser:0.1`.  Alternatively, to send the image straight to a remote container registry use

```
./gradlew jib -Djib.to.image=registry.example.com/my-service:latest
```

You can generate a "test harness" to try out your service as if it were running in the real ELG platform, using the ELG Python SDK:

```
# Create a virtual environment
python3 -m venv venv
. venv/bin/activate

# Install the ELG SDK
pip install elg

# Generate the test harness
elg local-installation docker --image simple-micronaut-tokeniser:0.1 \
      --execution_location http://localhost:8080/process \
      --name tokeniser \
      --gui \
      --folder local-test
```

- `--execution_location` by default Micronaut applications listen on `http://localhost:8080`.  The path component `/process` is taken from the `@Controller` annotation.
- `--gui` includes a web GUI in the test harness.  The default GUI works for annotation services such as this tokeniser, for other service types you may also need to add
  - `--gui_path index-mt.html` - for Machine Translation and other services that accept text and return more text
  - `--gui_path index-asr.html` - for speech recognition services that accept audio and return text

Now cd into the `local-test` folder and run `docker-compose up` - you should then be able to go to `http://localhost:8080` in a browser and test the service there.  You can also call it programmatically by POSTing plain text to `http://localhost:8080/execution/process/tokeniser`
