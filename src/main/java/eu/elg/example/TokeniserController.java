package eu.elg.example;

import eu.elg.ltservice.LTService;
import eu.elg.model.AnnotationObject;
import eu.elg.model.requests.TextRequest;
import eu.elg.model.responses.AnnotationsResponse;
import eu.elg.model.util.TextOffsetsHelper;
import io.micronaut.http.annotation.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller("/process")
public class TokeniserController extends LTService<TextRequest, LTService.Context> {

  private static final Pattern PATTERN = Pattern.compile("\\S+");

  @Override
  protected AnnotationsResponse handleSync(TextRequest request, Context ctx) {
    List<AnnotationObject> anns = new ArrayList<>();
    String content = request.getContent();
    Matcher m = PATTERN.matcher(content);

    // use TextOffsetsHelper to map between Java string indices from the Matcher
    // (which are UTF-16, so supplementary characters such as Emoji count as two
    // positions) and the ELG API annotations format (which counts in Unicode
    // characters, so supplementary code points count as one position).
    TextOffsetsHelper helper = new TextOffsetsHelper(content);
    
    while(m.find()) {
      anns.add(helper.annotationWithOffsets(m.start(), m.end()).withFeature("string", m.group()));
    }
    return new AnnotationsResponse().withAnnotations("Token", anns);
  }
}

